// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCpV7rzY5XKYOQCQh4TQIG4NZug09WYA5M',
    authDomain: 'controle-fin.firebaseapp.com',
    projectId: 'controle-fin',
    storageBucket: 'controle-fin.appspot.com',
    messagingSenderId: '282167834854',
    appId: '1:282167834854:web:2e8a1c120ba5c2cd872327',
    measurementId: 'G-GKYQ33Y8EM'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
