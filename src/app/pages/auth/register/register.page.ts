import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from 'controle/node_modules/@angular/forms/forms';
import { createSecureServer } from 'node:http2';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  loginForm: FormGroup;

  constructor(

    private builder: FormBuilder,
  private service: LoginService
  ) { }

  ngOnInit(
    this: any.loginForm = this.builder.group({
      snome:['', [Validators.required, Validators.minLength(2), Validators.maxLength(19) ]],
      snome:['', [Validators.required, Validators.minLength(2), Validators.maxLength(19) ]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirm_pass: ['', [Validators.required, Validators.minLength(8)]],
    });

   {
    
    createUser() {
      const user = this.loginForm.value;
      console.log(user);
    }
  }


