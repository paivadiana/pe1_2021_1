import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ForgotPage} from '.forgot/Forgot.Page';
import {LoginPage} from '.Login/login.Page';
import { RegisterPage } from './register/register.page';
import { CommonModule } from 'controle/node_modules/@angular/common/common';
import { IonicModule } from 'controle/node_modules/@ionic/angular/ionic-angular';
import { FormsModule, ReactiveFormsModule } from 'controle/node_modules/@angular/forms/forms';

const routes: Routes = [
  
  {
    path: '', children:  [
      {path: '', component: LoginPage},
      {path: 'forgot', component: ForgotPage},
      {path: 'register', component: RegisterPage},
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    LoginPage,
    ForgotPage,
    RegisterPage,
  ]
 
})
export class AuthRoutingModule { }
